#!/usr/bin/env bats

# Developer:
# ---------
# Name:      go2null
# GitHub:    https://github.com/go2null
# GitLab:    https://gitlab.com/go2null
# Docker:    https://hub.docker.com/u/go2null
#
# Description:
# -----------
# Simple test with Bats

@test "that we can execute bash inside the container" {
	run docker run -it go2null/pearl --help
  	[ "$status" -eq 0  ]
}

@test "that git is available" {
	run docker run -it go2null/pearl -c "[[ $(which git) ]]"
  	[ "$status" -eq 0  ]
}

@test "that grep is available" {
	run docker run -it go2null/pearl -c "[[ $(which grep) ]]"
  	[ "$status" -eq 0  ]
}

@test "that pearl is available" {
	run docker run -it go2null/pearl -c "[[ $(which pearl) ]]"
  	[ "$status" -eq 0  ]
}

@test "that the pearl home directory exists" {
	run docker run -it go2null/pearl -c "[[ -d /home/pearl/ ]]"
  	[ "$status" -eq 0  ]
}

@test "that the pearl temp directory doesn't exist" {
	run docker run -it go2null/pearl -c "[[ ! -d /tmp/pearl/ ]]"
  	[ "$status" -eq 0  ]
}

@test "that we run as an unprivileged user" {
	run docker run -it go2null/pearl -c "[[ ${UID} -ne 0 ]]"
  	[ "$status" -eq 0  ]
}

@test "that the current process is greater then pid 1" {
	run docker run -it go2null/pearl -c "[[ ${$} -gt 1 ]]"
  	[ "$status" -eq 0  ]
}

